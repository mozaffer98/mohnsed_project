import 'dart:io';

import 'package:mohaned_project/model/admin.dart';
import 'package:mohaned_project/model/family_member.dart';
import 'package:mohaned_project/model/suggestion.dart';
import 'package:mohaned_project/module/Admin/AdminLogin.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';


class DatabaseHelperMember
{
  static Database _db;

  final String TableName="Member";
  final String id="id";
  final String name="name";
  final String nationalNumber="nationalNumber";
  final String gender="gender";
  final String kinship="kinship";
  final String familynationalnumber="familynationalnumber";


  Future<Database> get db async
  {
    if(_db != null) {
      return _db;
    }
    else {
      _db = await init_db();
      return _db;
    }
  }

  init_db() async
  {
    Directory documentDiretory=await getApplicationDocumentsDirectory();
    String path=join(documentDiretory.path,'mydb1.db');
    var myOwnDb=await openDatabase(path,onCreate:_oncreate ,version: 1);
    print("created!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
    return myOwnDb;
  }

  void _oncreate(Database db,int newVersion)async {
    String sql=
        'CREATE TABLE $TableName('
        '$id INTEGER PRIMARY KEY ,'
        ' $name TEXT, '
        ' $nationalNumber INTEGER, '
        ' $kinship TEXT, '
        ' $familynationalnumber INTEGER, '
        '$gender TEXT'
        ')';

    String sql_adminAccount=
        'CREATE TABLE AdminAccount('
        'id INTEGER PRIMARY KEY ,'
        ' Username TEXT, '
        ' Password TEXT '
        ')';


    String sql_Suggestion=
        'CREATE TABLE Suggestion('
        'id INTEGER PRIMARY KEY ,'
        ' nationalnumber INTEGER, '
        ' suggestion TEXT '
        ')';

    await db.execute(sql);
    print("usertable");

    await db.execute(sql_adminAccount);
    print("adminAccount");

    await db.execute(sql_Suggestion);
    print("SuggestionTable");

  }

  Future<int> addSuggestion( Suggestion suggestion)async
  {
    var db_Client=await db;
    int result=await db_Client.insert("Suggestion", suggestion.toMap());
    return result;
  }


  Future<List> GetAllSuggestion()async
  {
    var db_Client=await db;
    var sqlquery="SELECT * FROM Suggestion  ";
    List result=await db_Client.rawQuery(sqlquery);
    return result.toList();
  }


  Future<int> addMember( FamilyMember familyMember)async
  {
    var db_Client=await db;
    int result=await db_Client.insert(TableName, familyMember.toMap());
    return result;
  }

  Future<List> GetAllMember(int national_number)async
  {
    var db_Client=await db;
    var sqlquery="SELECT * FROM $TableName WHERE $familynationalnumber = $national_number ";
    List result=await db_Client.rawQuery(sqlquery);
    return result.toList();
  }

  Future<int> GetCount()async
  {
    var db_Client=await db;
    var sqlquery="SELECT COUNT(*) FROM $TableName";
    return Sqflite.firstIntValue(await db_Client.rawQuery(sqlquery));
  }



  Future<FamilyMember> GetMember(int memberId)async
  {
    var db_Client=await db;
    var sqlquery="SELECT * FROM $TableName WHERE $id = $memberId";
    var result=await db_Client.rawQuery(sqlquery);
    if(result.length ==null)return null;
    return new FamilyMember.fromMap(result.first);
  }

  Future<int> deleteMember(int memberId)async
  {
    var db_Client=await db;
    return await db_Client.delete(TableName,where: "$id = ?",whereArgs: [memberId]);
  }

  Future<int> updateMember( FamilyMember familyMember)async
  {
    var db_Client=await db;
    return await db_Client.update(TableName, familyMember.toMap(), where: "$id = ? ",whereArgs: [familyMember.id]);
  }




  closeDb()async
  {
    var db_Client=await db;
    return await db_Client.close();
  }

}