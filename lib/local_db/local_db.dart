
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:mohaned_project/model/user.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';


class DatabaseHelper
{
  static Database _db;

  final String TableName="User";
  final String id="id";
  final String name="name";
  final String nationalNumber="nationalNumber";
  final String phoneNumber="phoneNumber";
  final String gender="gender";
  final String familyMember="familyMember";
  final String address="address";
  final String isfamily="isfamily";




  final String bank="bank";
  final String accountNumber="accountNumber";
  final String branchName="branchName";

  final String paymethod="paymethod";

  final String bankcardnumber="bankcardnumber";

  final String companyname="companyname";
  final String simnumber="simnumber";

  Future<Database> get db async
  {
    if(_db != null) {
      return _db;
    }
    else {
      _db = await init_db();
    return _db;
    }
  }

  init_db() async
  {
    Directory documentDiretory=await getApplicationDocumentsDirectory();
    String path=join(documentDiretory.path,'mydb.db');
    var myOwnDb=await openDatabase(path,onCreate:_oncreate ,version: 1);
    print("created!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
    return myOwnDb;
  }

 void _oncreate(Database db,int newVersion)async {
    String sql=
        'CREATE TABLE $TableName('
        '$id INTEGER PRIMARY KEY ,'
        ' $name TEXT, '
        ' $nationalNumber INTEGER, '
        '$phoneNumber INTEGER,'
        '$gender TEXT,'
        '$familyMember INTEGER,'
        '$address TEXT,'
        '$paymethod TEXT,'
        '$bank TEXT,'
        '$accountNumber INTEGER,'
        '$branchName TEXT,'
        '$bankcardnumber TEXT,'
        '$companyname TEXT,'
        '$isfamily BOOL,'
        '$simnumber TEXT'
        ')';
    await db.execute(sql);
  }


  Future<int> addUser( User user)async
  {
    var db_Client=await db;

      int result=await db_Client.insert(TableName, user.toMap());
      return result;

  }

  Future<List> GetAllUsers()async
  {
    var db_Client=await db;

    var sqlquery="SELECT * FROM $TableName";
    List result=await db_Client.rawQuery(sqlquery);
    return result.toList();
  }

  Future<int> GetCount()async
  {
    var db_Client=await db;
    var sqlquery="SELECT COUNT(*) FROM $TableName";
    return Sqflite.firstIntValue(await db_Client.rawQuery(sqlquery));
  }

  Future<List> CheckUser(int nationalnumber)async
  {
    var db_Client=await db;
    var sqlquery="SELECT * FROM $TableName WHERE $nationalNumber = $nationalnumber";
    var result=await db_Client.rawQuery(sqlquery);

    print(result);
   return [ result.length, if(result.length>0) User.fromMap(result.first).isfamily];


  }
  Future<User> GetUser(int nationalnumber)async
  {
    var db_Client=await db;

    var sqlquery="SELECT * FROM $TableName WHERE $nationalNumber = $nationalnumber";
    var result=await db_Client.rawQuery(sqlquery);

    print(result);
    return new User.fromMap(result.first) ;


  }


  Future<User> GetMyDetails(String _name)async
  {
    var db_Client=await db;
    var sqlquery="SELECT * FROM USER WHERE $name = $_name";
    var result=await db_Client.rawQuery(sqlquery);

    return User.fromMap(result.last);

  }

  Future<User> GetAllDetails(String _name)async
  {
    var db_Client=await db;
    var sqlquery="SELECT * FROM USER WHERE $name = $_name";
    var result=await db_Client.rawQuery(sqlquery);

    return User.fromMap(result.last);

  }



  Future<int> updateUser( User user)async
  {
    var db_Client=await db;
    return await db_Client.update(TableName, user.toMap(), where: "$id = ? ",whereArgs: [user.id]);
  }


  Future<int> deleteAllProduct()async
  {
    var db_Client=await db;
    var sqlquery="DELETE  FROM $TableName";
    var result=await db_Client.rawDelete(sqlquery);
    if(result ==null) return null;
    else return 1;

  }


  closeDb()async
  {
    var db_Client=await db;
    return await db_Client.close();
  }

}