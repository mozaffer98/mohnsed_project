import 'package:flutter/material.dart';
import 'package:mohaned_project/module/User/add_member.dart';

class MemberItem extends StatelessWidget {
  final name,nationalNumber,kinship,gender;
  final VoidCallback onPressRemove,onPressEdit;
  final bool isAdmin;
  const MemberItem({ this.name, this.nationalNumber, this.kinship, this.gender, this.onPressRemove, this.onPressEdit, this.isAdmin}) ;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child:order(context) ,
    );
  }

  Widget order(BuildContext ctx)
  {
    final size = MediaQuery.of(ctx).size;
    return Padding(
      padding: EdgeInsetsDirectional.only(
        start: size.width * .05,
        end: size.width * .05,
        top: size.height * .02,
      ),
      child: Material(
        elevation: 10,
        borderRadius: BorderRadius.circular(15),
        child: SizedBox(
          width: size.width,
          height: size.height * .210,
          child: Row(
             textDirection: TextDirection.rtl,
            children: [
              Expanded(
                flex: 1,
                child: Column(
                  textDirection: TextDirection.rtl,
                  children: [
                    Padding(
                      padding: EdgeInsetsDirectional.only(
                        top: size.height * .01,
                      ),
                      child: const CircleAvatar(
                        radius: 25,
                        backgroundImage: AssetImage("assets/man.jpg"),
                      ),

                    ),
                    Text(
                      kinship,
                      style:TextStyle(fontWeight: FontWeight.bold,),
                    ),
                  ],
                ),
              ),
              Expanded(
                flex: 2,
                child: Container(
                  child: Column(
                    textDirection: TextDirection.rtl,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(
                          vertical: size.height * .01,
                        ),
                        child: Text(
                          name,
                          style:TextStyle(fontWeight: FontWeight.bold,),
                        ),
                      ),
                      Row(
                        textDirection: TextDirection.rtl,

                        children: [
                          Column(
                            children: [
                              Container(
                                width: size.width * .05,
                                height: size.width * .05,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15),
                                  color: const Color(0xFF31B89C),
                                ),
                              ),
                              Container(
                                width: 3,
                                height: size.height * .04,
                                color: Colors.grey[400],
                              ),
                              Container(
                                width: size.width * .05,
                                height: size.width * .05,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15),
                                  color: Color(0xffEE4B5B),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                            width: size.width * .02,
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Wrap(
                                  direction: Axis.horizontal, //Vertical || Horizontal
                                  children: [
                                    Text(
                                      "${nationalNumber}",
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 1,
                                      style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold,
                                        color: Color(0xFF1C2D57),
                                      ),
                                    ),]
                              ),
                              SizedBox(
                                height: size.height * .05,
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 30),
                                child: Text(
                                  "${gender}",
                                  style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.bold,
                                    color: Color(0xFF1C2D57),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Column(
                  textDirection: TextDirection.rtl,
                  children: [
                    Padding(
                      padding: EdgeInsetsDirectional.only(
                        top: size.height * .01,
                      ),
                      child:IconButton(onPressed: onPressRemove, icon: Icon(Icons.highlight_remove,color:Color(0xffEE4B5B) ,)),

                    ),

                    Padding(
                      padding: EdgeInsetsDirectional.only(
                        top: size.height * .01,
                      ),
                       child:  isAdmin==true? IconButton(onPressed: onPressEdit, icon: Icon(Icons.edit,color:Color(0xFF31B89C) ,)):Container(),
                    ),
                  ],
                ),
              ),



            ],
          ),
        ),
      ),
    );
  }
}

