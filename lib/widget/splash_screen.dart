
import 'package:flutter/material.dart';
import 'package:mohaned_project/local_db/member_local_db.dart';
import 'package:mohaned_project/main.dart';
import 'package:mohaned_project/model/admin.dart';
import 'package:mohaned_project/widget/background_image.dart';
import 'package:mohaned_project/widget/gradual.dart';
import 'package:shared_preferences/shared_preferences.dart';


class SplashScreen extends StatefulWidget {
  const SplashScreen({Key key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  // String? token;
  //


  @override
  void initState() {
    super.initState();
    // token = "";


  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    Future.delayed(
      const Duration(seconds: 4),
          () {
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext ctx)=> HomePage()), (route) => false);
      },
    );
    return Scaffold(
      body: SafeArea(
        child: Container(
          width: size.width,
          height: size.height,
          child: BackgroundImage(),
        )
      ),
    );
  }
}