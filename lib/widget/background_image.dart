import 'package:flutter/material.dart';

class BackgroundImage extends StatelessWidget {
  const BackgroundImage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 250,
      decoration: const BoxDecoration(
        image: DecorationImage(
          image: AssetImage(
            'assets/logo.jpeg',
          ),
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
class BackgroundImage1 extends StatelessWidget {
  const BackgroundImage1({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        image: DecorationImage(
          image: AssetImage(
            'assets/background.jpeg',
          ),
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
