import 'package:flutter/material.dart';

class gradual extends StatelessWidget {
  final color,rowcolor;
   gradual({Key key, this.color, this.rowcolor}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Container(
      color: color,
      height: size.height * .25,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Expanded(
                  child: Container(
                width: size.width,
                height: size.height * .09,
                color:rowcolor,
              ))
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Expanded(
                  child: Container(
                    margin: EdgeInsets.only(right: size.width*.25,),
                    width: size.width,
                    height: size.height * .055,
                    color: rowcolor,
                  ))
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Expanded(
                  child: Container(
                    margin: EdgeInsets.only(right: size.width*.60),
                    width: size.width,
                    height: size.height * .05,
                    color: rowcolor,
                  ))
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Expanded(
                  child: Container(
                    margin: EdgeInsets.only(right: size.width*.85),
                    width: size.width,
                    height: size.height * .04,
                    color:rowcolor ,
                  ))
            ],
          ),
        ],
      ),
    );
  }
}
