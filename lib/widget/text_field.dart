import 'package:flutter/material.dart';

class DefaultTextFormField extends StatelessWidget {
  final TextEditingController controller;

  final String hint;
  final  validate,onchange;
  final Widget prefixIcon;
  final Widget prefix;
  final double horizontalPadding;
  final double verticalPadding;
  final TextInputType type;


  DefaultTextFormField(
      {this.controller,
      this.hint,
      this.validate,
      this.prefixIcon,
      this.prefix,
      this.horizontalPadding,
      this.verticalPadding,
      this.type, this.onchange});

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Padding(
      padding: EdgeInsets.symmetric(
        // horizontal: horizontalPadding ?? size.width * .06,
        // vertical: verticalPadding ?? size.height * .035,
      ),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8),
        ),
        height: size.height * .07,
        child: TextFormField(
          controller: controller,
          validator: validate,
          textDirection: TextDirection.rtl,
          keyboardType: type ?? TextInputType.phone,
          decoration: InputDecoration(
            border: const OutlineInputBorder(),
            hintText: hint,
            hintStyle: const TextStyle(
              fontFamily: 'Tajawal',
            ),
            prefixIcon: prefixIcon,
            prefix: prefix,
          ),
        ),
      ),
    );
  }
}
class DefaultTextFormField1 extends StatelessWidget {
  final TextEditingController controller;

  final String hint;
  final String Function(String) validate;
  final Widget prefixIcon;
  final Widget prefix;
  final double horizontalPadding;
  final double verticalPadding;
  final TextInputType type;


  DefaultTextFormField1(
      {this.controller,
      this.hint,
      this.validate,
      this.prefixIcon,
      this.prefix,
      this.horizontalPadding,
      this.verticalPadding,
      this.type});

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: horizontalPadding ?? size.width * .06,
        vertical: verticalPadding ?? size.height * .035,
      ),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(8),
        ),
        height: size.height * .07,
        child: TextFormField(
          maxLines: 10,
          textAlignVertical: TextAlignVertical.bottom,
          controller: controller,
          validator: validate,
          keyboardType: type ?? TextInputType.phone,
          decoration: InputDecoration(
            border: const OutlineInputBorder(),
            hintText: hint,
            hintStyle: const TextStyle(
              fontFamily: 'Tajawal',
            ),
            prefixIcon: prefixIcon,
            prefix: prefix,
          ),
        ),
      ),
    );
  }
}
