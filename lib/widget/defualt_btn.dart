import 'package:flutter/material.dart';
import 'package:mohaned_project/main.dart';

class DefaultButton extends StatelessWidget {
  final double width;
  final double borderRadius;
  final String text;
   final Color textColor;
  final VoidCallback onPress;

  const DefaultButton({
    Key key,
    this.width,
    this.borderRadius = 8.0,
     this.text,

    this.textColor = Colors.white,
     this.onPress,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Padding(
      padding: EdgeInsets.only(
        left: size.width*.40 ,
            right: 0
      ),
      child: Container(
        width: size.width *.50,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(topLeft: Radius.circular(8),bottomLeft: Radius.circular(8)),
          color:textColor ,
        ),
        child: MaterialButton(
          onPressed: onPress,
          child: Text(
            text,
            style: const TextStyle(
              color: Colors.white,
              fontSize: 16,
              fontFamily: 'Tajawal',
              fontWeight: FontWeight.bold
            ),
          ),
        ),
      ),
    );
  }
}
