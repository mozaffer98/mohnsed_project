import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:mohaned_project/local_db/local_db.dart';
import 'package:mohaned_project/module/Admin/AdminLogin.dart';
import 'package:mohaned_project/module/Admin/family_list.dart';
import 'package:mohaned_project/module/Setting.dart';
import 'package:mohaned_project/module/User/register_type.dart';
import 'package:mohaned_project/module/User/user_details.dart';
import 'package:mohaned_project/widget/background_image.dart';
import 'package:mohaned_project/widget/defualt_btn.dart';
import 'package:mohaned_project/widget/gradual.dart';
import 'package:mohaned_project/widget/splash_screen.dart';
import 'package:mohaned_project/widget/text_field.dart';

void main() {
  runApp(const MyApp());
}

Color   MainColor=Color(0xFFEE4B5B);

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: '',
      theme: ThemeData(
      ),
      locale: Locale("ar"),
      home: const SplashScreen(),
      builder: EasyLoading.init(),

    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({Key key,  }) : super(key: key);
  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  final formKey = GlobalKey<FormState>();
  TextEditingController _national_number_ctrl=TextEditingController();


  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return
      Scaffold(
        key: scaffoldKey,
        appBar: AppBar(
          title: Text("الرئيسية",style: TextStyle(color: Colors.black54,fontWeight: FontWeight.bold),),
          centerTitle: true,
          elevation: 2,
          backgroundColor: Colors.white,
          leading:  GestureDetector(
            onTap: ()=> Navigator.push(context, MaterialPageRoute(builder: (BuildContext ctx)=>AdminLogin())),
            child: Container(
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(
                    'assets/logo.jpeg',
                  ),
                  fit: BoxFit.cover,
                ),
              ),

            ),
          ),
        ),
        backgroundColor: Colors.white,
        body: Stack(
          alignment: AlignmentDirectional.centerStart,
          children: [
            const BackgroundImage1(),
            SingleChildScrollView(
              child: Form(
                key: formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(top: size.height*.40,right: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text("قم بإدخال الرقم الوطني",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(top: 20,right: 20,left: 20),
                      child: DefaultTextFormField(
                        controller: _national_number_ctrl,
                        hint: "",
                        validate: (value) {if (value == null) {
                            return 'ادخل الرقم الوطني';
                          }return null;},
                        onchange:  (value) {
                         if(value.length > 2)
                           {
                             value = '$value -';
                             setState(() {
                             });
                           }

                        },
                      ),
                    ),

                    Container(
                        padding: EdgeInsets.only(top: 85,),

                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                DefaultButton(
                                  onPress: ()
                                  {
                                    if (_national_number_ctrl.text.isEmpty) {
                                      scaffoldKey.currentState.showSnackBar(
                                        SnackBar(
                                          content: Text("ادخل الرقم الوطني "),
                                        ),
                                      );
                                    }
                                    else if(_national_number_ctrl.text.length < 14||_national_number_ctrl.text.length>14)
                                      {
                                        scaffoldKey.currentState.showSnackBar(
                                          SnackBar(
                                            content: Text("الرقم الوطني يجب ان يكون 14 حقل"),
                                          ),
                                        );
                                      }
                                    else
                                    {
                                      GetUser();
                                    }
                                  },
                                  text: "التالي",
                                  textColor:Color(0xFF31B89C) ,
                                  width: 1,)
                              ],
                            ),
                            SizedBox(height: 5,),
                            // Row(
                            //   mainAxisAlignment: MainAxisAlignment.end,
                            //   children: [
                            //     DefaultButton(
                            //       onPress: (){
                            //         Navigator.push(context, MaterialPageRoute(builder: (BuildContext ctx)=>Setting()));
                            //       },
                            //       text: "التعديل والشكاوي",
                            //       textColor:Color(0xffEE4B5B) ,
                            //       width: 1,)
                            //   ],
                            // ),


                          ],
                        ))

                  ],
                ),
              ),
            ),
          ],
        ),
      );

  }

  var db=new DatabaseHelper();

  void GetUser()async
  {
    List res =await db.CheckUser(int.parse(_national_number_ctrl.text));

    if(res.elementAt(0) == null || res.elementAt(0) == 0) {
      Navigator.push(context, MaterialPageRoute(builder: (BuildContext ctx)=>RegisterType(national_number:int.parse( _national_number_ctrl.text))));
    }
    else
      {
        Navigator.push(context, MaterialPageRoute(builder: (BuildContext ctx)=>Setting(nationalnumber:int.parse( _national_number_ctrl.text),isFamily: res.elementAt(1)==0?false:true,)));
      }
  }
}


