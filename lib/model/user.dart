class User {
  int id;
  int nationalNumber;
  String name;
  int phoneNumber;
  String gender;
  int familyMember;
  String address;
  int isfamily;

  String paymethod;//f
  String bank;
  int accountNumber;
  String branchName;
  String bankcardnumber;//f
  String companyname;//f
  String simnumber;

  User(
      {this.id,
      this.nationalNumber,
      this.name,
      this.phoneNumber,
      this.gender,
      this.familyMember,
      this.address,
        this.isfamily,
      this.paymethod,
      this.bank,
      this.accountNumber,
      this.branchName,
      this.bankcardnumber,
      this.companyname,
      this.simnumber}); //f




  int get _id => id;
  int get _nationalNumber => nationalNumber;
  String get _name => name;
  int get _phoneNumber =>   phoneNumber;
  String get _gender =>   gender;
  int get _familyMember =>   familyMember;
  String get _address =>   address;
  String get _bank =>   bank;
  int get _accountNumber =>   accountNumber;
  String get _branchName =>   branchName;

  String get _paymethod =>   paymethod;
  String get _bankcardnumber =>   bankcardnumber;
  String get _companyname =>   companyname;
  String get _simnumber =>   simnumber;

  User.fromMap(Map<String, dynamic> json) {
    id = json['id'];
    nationalNumber = json['nationalNumber'];
    name = json['name'];
    phoneNumber = json['phoneNumber'];
    gender = json['gender'];
    familyMember = json['familyMember'];
    address = json['address'];
    isfamily = json['isfamily'];

    paymethod = json['paymethod'];
    bank = json['bank'];
    accountNumber = json['accountNumber'];
    branchName = json['branchName'];

    bankcardnumber = json['bankcardnumber'];
    companyname = json['companyname'];
    simnumber = json['simnumber'];
  }


  Map<String, dynamic> toMap() {
    final Map<String, dynamic> map = new Map<String, dynamic>();

    if(id !=null) map['id'] = _id;
    map['nationalNumber'] = _nationalNumber;
    map['name'] = _name;
    map['phoneNumber'] = _phoneNumber;
    map['gender'] = _gender;
    map['familyMember'] = _familyMember;
    map['address'] = _address;
    map['isfamily'] = this.isfamily;

    map['bank'] = _bank;
    map['accountNumber'] = _accountNumber;
    map['branchName'] = _branchName;

    map['paymethod'] = _paymethod;
    map['bankcardnumber'] = _bankcardnumber;
    map['companyname'] = _companyname;
    map['simnumber'] = _simnumber;
    return map;
  }
}
