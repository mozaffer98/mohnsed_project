class FamilyMember {
  int id;
  String name;
  int nationalNumber;
  String kinship;
  String gender;
  int familynationalnumber;

  FamilyMember({this.id, this.name, this.nationalNumber, this.kinship, this.gender,this.familynationalnumber});


  int get _id => id;
  int get _nationalNumber => nationalNumber;
  String get _name => name;
  String get _kinship => kinship;
  String get _gender =>   gender;
  int get _familynationalnumber=>   familynationalnumber;

  FamilyMember.fromMap(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    nationalNumber = json['nationalNumber'];
    kinship = json['kinship'];
    gender = json['gender'];
    familynationalnumber = json['familynationalnumber'];
  }

  Map<String, dynamic> toMap() {
    final Map<String, dynamic> map = new Map<String, dynamic>();
    if(id !=null)  map['id'] = _id;
    map['name'] = _name;
    map['nationalNumber'] = _nationalNumber;
    map['kinship'] = _kinship;
    map['gender'] = _gender;
    map['familynationalnumber'] = _familynationalnumber;
    return map;
  }
}
