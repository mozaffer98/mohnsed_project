class Suggestion {
  int id;
  int nationalnumber;
  String suggestion;

  Suggestion({this.id, this.nationalnumber, this.suggestion});

  Suggestion.frommap(Map<String, dynamic> json) {
    id = json['id'];
    nationalnumber = json['nationalnumber'];
    suggestion = json['suggestion'];
  }

  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['nationalnumber'] = this.nationalnumber;
    data['suggestion'] = this.suggestion;
    return data;
  }
}
