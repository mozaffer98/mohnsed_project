import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mohaned_project/main.dart';
import 'package:mohaned_project/module/User/add_member.dart';
import 'package:mohaned_project/module/User/bank_details.dart';
import 'package:mohaned_project/module/User/member_list.dart';
import 'package:mohaned_project/module/User/profile.dart';
import 'package:mohaned_project/module/User/suggestions.dart';
import 'package:mohaned_project/module/User/user_details.dart';
import 'package:mohaned_project/widget/background_image.dart';

class Setting extends StatefulWidget {
  final nationalnumber;
  final bool isFamily;
  const Setting({Key key, this.nationalnumber, this.isFamily}) : super(key: key);

  @override
  _SettingState createState() => _SettingState();
}

class _SettingState extends State<Setting> {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return   Scaffold(
      appBar: AppBar(
        title: Text("الضبط",style: TextStyle(color: Colors.black54,fontWeight: FontWeight.bold),),
        centerTitle: true,
        elevation: 2,
        backgroundColor: Colors.white,
        leading:  Container(
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage(
                'assets/logo.jpeg',
              ),
              fit: BoxFit.cover,
            ),
          ),

        ),
        actions: [
          IconButton(onPressed: (){
            Navigator.push(context, MaterialPageRoute(builder: (BuildContext ctx)=>UserDetails(isRegister: false,nationalnumber: widget.nationalnumber,reg_type:widget.isFamily?"family":"single" ,)));
          }, icon: Icon(Icons.settings,color: Color(0xFF31B89C),)),
          IconButton(onPressed: (){
            showAlert(context, "هل تريد تسجيل الخروج");
          }, icon: Icon(Icons.logout,color: Color(0xFF31B89C),)),
        ],
      ),
      body: Stack(
        alignment: AlignmentDirectional.centerStart,
        children: [
          const BackgroundImage1(),
          SingleChildScrollView(
            child: Form(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(top: size.height*.07),
                          height:size.height*.70,
                          width:size.width ,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                          if(widget.isFamily)    Container(
                                  padding: EdgeInsets.only(top: size.height*.004,right: size.width*.09),
                                  //    width:size.width*.75 ,

                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      Container(
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(15),
                                          color: Color(0xFF31B89C),

                                        ),
                                        width:size.width*.50 ,
                                        child: TextButton(
                                          child: Text(
                                              "التسجيل للأفراد".toUpperCase(),
                                              style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold)
                                          ),
                                          style: ButtonStyle(

                                              padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.all(15)),
                                              foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                                              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                                  RoundedRectangleBorder(
                                                      borderRadius: BorderRadius.only(bottomRight: Radius.circular(15),topRight: Radius.circular(8)),
                                                      side: BorderSide(color: Colors.white)
                                                  )
                                              )
                                          ),
                                          onPressed: () =>  Navigator.push(context, MaterialPageRoute(builder: (BuildContext ctx)=>AddMember(isAdd: true,nationalnumber: widget.nationalnumber,isRegister: false,))),
                                        ),
                                      ),
                                    ],
                                  )
                              ),
                              // Container(
                              //     padding: EdgeInsets.only(top: size.height*.004,right: size.width*.09),
                              //     //    width:size.width*.75 ,
                              //
                              //     child: Row(
                              //       mainAxisAlignment: MainAxisAlignment.end,
                              //       children: [
                              //         Container(
                              //           decoration: BoxDecoration(
                              //             borderRadius: BorderRadius.circular(15),
                              //             color: Color(0xFF31B89C),
                              //
                              //           ),
                              //           width:size.width*.50 ,
                              //           child: TextButton(
                              //             child: Text(
                              //                 "التسجيل للعائلة".toUpperCase(),
                              //                 style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold)
                              //             ),
                              //             style: ButtonStyle(
                              //
                              //                 padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.all(15)),
                              //                 foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                              //                 shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                              //                     RoundedRectangleBorder(
                              //                         borderRadius: BorderRadius.only(bottomRight: Radius.circular(15),topRight: Radius.circular(8)),
                              //                         side: BorderSide(color: Colors.white)
                              //                     )
                              //                 )
                              //             ),
                              //             onPressed: () =>  Navigator.push(context, MaterialPageRoute(builder: (BuildContext ctx)=>UserDetails(isRegister: true,nationalnumber: widget.nationalnumber.toString(),))),
                              //           ),
                              //         ),
                              //       ],
                              //     )
                              // ),
                              Container(
                                  padding: EdgeInsets.only(top: size.height*.004,right: size.width*.09),
                                  //    width:size.width*.75 ,

                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      Container(
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(15),
                                          color: Color(0xFF31B89C),

                                        ),
                                        width:size.width*.50 ,
                                        child: TextButton(
                                          child: Text(
                                              "تفصيلي".toUpperCase(),
                                              style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold)
                                          ),
                                          style: ButtonStyle(

                                              padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.all(15)),
                                              foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                                              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                                  RoundedRectangleBorder(
                                                      borderRadius: BorderRadius.only(bottomRight: Radius.circular(15),topRight: Radius.circular(8)),
                                                      side: BorderSide(color: Colors.white)
                                                  )
                                              )
                                          ),
                                          onPressed: () =>  Navigator.push(context, MaterialPageRoute(builder: (BuildContext ctx)=>UserProfile(nationalnumber: widget.nationalnumber,))),
                                        ),
                                      ),
                                    ],
                                  )
                              ),
                              Container(
                                  padding: EdgeInsets.only(top: size.height*.004,right: size.width*.09),
                                  //    width:size.width*.75 ,

                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      Container(
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(15),
                                          color: Color(0xFF31B89C),

                                        ),
                                        width:size.width*.50 ,
                                        child: TextButton(
                                          child: Text(
                                              "تقديم شكوى".toUpperCase(),
                                              style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold)
                                          ),
                                          style: ButtonStyle(

                                              padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.all(15)),
                                              foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                                              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                                  RoundedRectangleBorder(
                                                      borderRadius: BorderRadius.only(bottomRight: Radius.circular(15),topRight: Radius.circular(8)),
                                                      side: BorderSide(color: Colors.white)
                                                  )
                                              )
                                          ),
                                          onPressed: () =>  Navigator.push(context, MaterialPageRoute(builder: (BuildContext ctx)=>Suggestions(nationalnumber: widget.nationalnumber,))),
                                        ),
                                      ),
                                    ],
                                  )
                              ),


                            ],
                          ),

                        )


                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
      floatingActionButton: widget.isFamily? FloatingActionButton(
          onPressed: ()=>Navigator.push(context, MaterialPageRoute(builder: (BuildContext ctx)=>MemberList(nationalnumber: widget.nationalnumber,isAdmin: true,))),
        backgroundColor: Color(0xFF31B89C),
        child: Icon(Icons.menu,color: Colors.white,),
        tooltip: "افراد الاسرة",
      ):Container(),
    );
  }

  void showAlert(BuildContext context,String massage) {
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
          content:Text(massage,textDirection: TextDirection.rtl,),
          actions: [
            const SizedBox(
              width: 10,
            ),
            Center(
              child: Container(
                alignment: Alignment.center,
                width: MediaQuery.of(context).size.width * .60,
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    TextButton(
                        onPressed: (){
                          Navigator.pop(context);
                        },
                        child: Text('لا',style: TextStyle(color: Color(0xffEE4B5B)),)),
                    SizedBox(width: 2,),

                    TextButton(
                        onPressed: ()async{
                          Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext ctx)=>HomePage()), (route) => false);
                        },
                        child: Text('نعم',style: TextStyle( color:Color(0xFF31B89C),
                        ),)),
                  ],
                ),
              ),
            ),
          ],
          // backgroundColor: AppBrand.mainColor,
        ));
  }
}
