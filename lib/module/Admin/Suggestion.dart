import 'package:flutter/material.dart';
import 'package:mohaned_project/model/suggestion.dart';

import '../../local_db/member_local_db.dart';

class SuggestionAdmin extends StatefulWidget {
  const SuggestionAdmin({Key key}) : super(key: key);

  @override
  _SuggestionAdminState createState() => _SuggestionAdminState();
}

class _SuggestionAdminState extends State<SuggestionAdmin> {

  var db=new DatabaseHelperMember();

  List<Suggestion> _suggestionlist;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    GetAllSuggestion();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("الشكاوي",style: TextStyle(color: Colors.black54,fontWeight: FontWeight.bold),),
        centerTitle: true,
        elevation: 2,
        backgroundColor: Colors.white,
        leading:  Container(
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage(
                'assets/logo.jpeg',
              ),
              fit: BoxFit.cover,
            ),
          ),

        ),
      ),
      body: _suggestionlist == null? Center(child: CircularProgressIndicator(),):
      Column(
        children: [
          Expanded(
            child: ListView.builder(
              shrinkWrap: true,
              physics: const BouncingScrollPhysics(),
              itemCount:_suggestionlist.length,
              itemBuilder: (context, index) =>    Card(
                elevation: 5,
                margin: EdgeInsets.only(top: 50,right: 20,left: 20),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(child: Text("${_suggestionlist.elementAt(index).nationalnumber}")),
                          Container(child: Text("الرقم الوطني")),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(child: Text("${_suggestionlist.elementAt(index).suggestion}")),
                          Container(child: Text("الشكوى")),
                        ],
                      ),
                    ),
                  ],
                ),
              )

            ),
          ),
        ],
      )

    );
  }

  void GetAllSuggestion()async
  {
    var list=await db.GetAllSuggestion();
    _suggestionlist=list.map((model) => Suggestion.frommap(model)).toList();
    setState(() {});
  }
}
