import 'package:flutter/material.dart';
import 'package:mohaned_project/main.dart';
import 'package:mohaned_project/module/Admin/Suggestion.dart';
import 'package:mohaned_project/module/Admin/family_list.dart';
import 'package:mohaned_project/widget/background_image.dart';

class AdminSetting extends StatefulWidget {
  const AdminSetting({Key key}) : super(key: key);

  @override
  _AdminSettingState createState() => _AdminSettingState();
}

class _AdminSettingState extends State<AdminSetting> {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        title: Text("مدير النظام",style: TextStyle(color: Colors.black54,fontWeight: FontWeight.bold),),
        centerTitle: true,
        elevation: 2,
        backgroundColor: Colors.white,
        actions: [
          IconButton(onPressed: (){
            showAlert(context, "هل تريد تسجيل الخروج؟");
          }, icon: Icon(Icons.logout,color: Color(0xFF31B89C),)),
        ],
        leading:  Container(
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage(
                'assets/logo.jpeg',
              ),
              fit: BoxFit.cover,
            ),
          ),

        ),
      ),
      body: Stack(
        alignment: AlignmentDirectional.topCenter,
        children: [
          const BackgroundImage1(),
          SingleChildScrollView(
            child: Form(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        Image.asset(
                          'assets/logo.jpeg',
                          height: size.height*.30,
                          width: size.width,
                        ),
                        Container(
                          padding: EdgeInsets.only(top: size.height*.004,right: size.width*.09),
                          //    width:size.width*.75 ,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15),
                                  color: Color(0xFF31B89C),

                                ),
                                width:size.width*.50 ,
                                child: TextButton(
                                  child: Text(
                                      "كل الاسر".toUpperCase(),
                                      style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold)
                                  ),
                                  style: ButtonStyle(

                                      padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.all(15)),
                                      foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                          RoundedRectangleBorder(
                                              borderRadius: BorderRadius.only(bottomRight: Radius.circular(15),topRight: Radius.circular(8)),
                                              side: BorderSide(color: Colors.white)
                                          )
                                      )
                                  ),
                                 onPressed: () =>  Navigator.push(context, MaterialPageRoute(builder: (BuildContext ctx)=>FamilyList())),
                                ),
                              ),
                            ],
                          )
                      ),
                        Container(
                            padding: EdgeInsets.only(top: size.height*.004,right: size.width*.09),
                            //    width:size.width*.75 ,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15),
                                    color: Color(0xFF31B89C),

                                  ),
                                  width:size.width*.50 ,
                                  child: TextButton(
                                    child: Text(
                                        "الشكاوي".toUpperCase(),
                                        style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold)
                                    ),
                                    style: ButtonStyle(

                                        padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.all(15)),
                                        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
                                        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                            RoundedRectangleBorder(
                                                borderRadius: BorderRadius.only(bottomRight: Radius.circular(15),topRight: Radius.circular(8)),
                                                side: BorderSide(color: Colors.white)
                                            )
                                        )
                                    ),
                                      onPressed: () =>  Navigator.push(context, MaterialPageRoute(builder: (BuildContext ctx)=>SuggestionAdmin())),
                                  ),
                                ),
                              ],
                            )
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
  void showAlert(BuildContext context,String massage) {
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
          content:Text(massage,textDirection: TextDirection.rtl,),
          actions: [
            const SizedBox(
              width: 10,
            ),
            Center(
              child: Container(
                alignment: Alignment.center,
                width: MediaQuery.of(context).size.width * .60,
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    TextButton(
                        onPressed: (){
                          Navigator.pop(context);
                        },
                        child: Text('لا',style: TextStyle(color: Color(0xffEE4B5B)),)),
                    SizedBox(width: 2,),

                    TextButton(
                        onPressed: ()async{
                          Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext ctx)=>HomePage()), (route) => false);
                        },
                        child: Text('نعم',style: TextStyle( color:Color(0xFF31B89C),
                        ),)),
                  ],
                ),
              ),
            ),
          ],
          // backgroundColor: AppBrand.mainColor,
        ));
  }
}
