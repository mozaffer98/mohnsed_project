import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:mohaned_project/local_db/member_local_db.dart';
import 'package:mohaned_project/module/Admin/admin_setting.dart';
import 'package:mohaned_project/module/Admin/family_list.dart';
import 'package:mohaned_project/widget/background_image.dart';
import 'package:mohaned_project/widget/defualt_btn.dart';
import 'package:mohaned_project/widget/text_field.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AdminLogin extends StatefulWidget {
    AdminLogin({Key key}) : super(key: key);

  @override
  _AdminLoginState createState() => _AdminLoginState();
}

class _AdminLoginState extends State<AdminLogin> {

  TextEditingController _username_ctrl = TextEditingController();
  TextEditingController _password_ctrl = TextEditingController();
  String _username,_password;
  var db=new DatabaseHelperMember();
  SharedPreferences prefs;
  Future<Null> registerAdmin() async {
     prefs = await SharedPreferences.getInstance();
    prefs.setString("username", "Admin");
    prefs.setString("password", "1234");

    setState(() {});
  }
  @override
  void initState() {
    super.initState();
    registerAdmin();
  }
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        title: Text("مدير النظام",style: TextStyle(color: Colors.black54,fontWeight: FontWeight.bold),),
        centerTitle: true,
        elevation: 2,
        backgroundColor: Colors.white,
        leading:  Container(
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage(
                'assets/logo.jpeg',
              ),
              fit: BoxFit.cover,
            ),
          ),

        ),
      ),
      body: Stack(
        alignment: AlignmentDirectional.centerStart,
        children: [
          const BackgroundImage1(),
           SingleChildScrollView(
            //دا اللى هيشغلى اللى  validator
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  padding: EdgeInsets.only(
                      top: size.height * .15, right: size.width * .1),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(
                        "اسم المستخدم",
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(
                      top: size.height * .007,
                      right: size.width * .1,
                      left: size.width * .1),
                  child: DefaultTextFormField(
                    controller: _username_ctrl,
                    type: TextInputType.name,
                    hint: "",
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(
                      top: size.height * .012, right: size.width * .1),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(
                        "كلمة المرور",
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(
                      top: size.height * .007,
                      right: size.width * .1,
                      left: size.width * .1),
                  child: DefaultTextFormField(
                    controller: _password_ctrl,
                    hint: "",
                  ),
                ),

                Container(
                    padding: EdgeInsets.only(
                      top: size.height * .099,
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            DefaultButton(
                              onPress: vaildation,
                              textColor: Color(0xFF31B89C),
                              text: "دخول",
                              width: 1,
                            )
                          ],
                        ),
                        SizedBox(
                          height: 5,
                        ),
                      ],
                    ))
              ],
            ),
          ),
        ],
      ),
    );
  }
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  void vaildation() async {
    if (_username_ctrl.text.isEmpty &&
        _password_ctrl.text.isEmpty ) {
      scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text("لا يمكن ترك حقل فارغ"),
        ),
      );
    } else if (_username_ctrl.text.isEmpty) {
      scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text("ادخل اسم المستخدم "),
        ),
      );
    } else if (_password_ctrl.text.isEmpty) {
      scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text("حقل كلمة المرور فارغ"),
        ),
      );
    }
    else {
    submit();
    }
  }

  void submit()async {
    EasyLoading.show(status: "...");
    _username=prefs.getString("username");
    print(_username);
    _password=prefs.getString("password");
    print(_password);

    if(_username_ctrl.text != _username || _password_ctrl.text != _password)
    {
      EasyLoading.showError("خطأ اسم المستخدم او كلمة المرور");
    }
    else
      {
        EasyLoading.dismiss();
        Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext ctx)=>AdminSetting()), (route) => false);
      }
  }

}
