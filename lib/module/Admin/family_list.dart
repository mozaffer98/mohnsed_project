import 'package:flutter/material.dart';
import 'package:mohaned_project/local_db/local_db.dart';
import 'package:mohaned_project/model/user.dart';
 import 'package:mohaned_project/module/User/member_list.dart';
import 'package:mohaned_project/module/User/profile.dart';
import 'package:mohaned_project/widget/text_field.dart';

class FamilyList extends StatefulWidget {
  const FamilyList({Key key}) : super(key: key);

  @override
  _FamilyListState createState() => _FamilyListState();
}

class _FamilyListState extends State<FamilyList> {

  var db = new DatabaseHelper();
  List<User> _user,searcheduser;

  List<String> allNames = [];
  // searched names
  List<String> searchedNames = [];


  var controller=TextEditingController();
 // List<String> names;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getUserDetails();

}
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("كل الاسر",style: TextStyle(color: Colors.black54,fontWeight: FontWeight.bold),),
          centerTitle: true,
          elevation: 2,
          backgroundColor: Colors.white,
          leading:  Container(
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage(
                  'assets/logo.jpeg',
                ),
                fit: BoxFit.cover,
              ),
            ),

          ),
          // actions: [
          //   IconButton(onPressed: (){
          //     showSearch(context: context, delegate: MySearhDelegate(_user));
          //   }, icon: Icon(Icons.search,color: Color(0xFF31B89C),))
          // ],
        ),
        body: searcheduser == null? Center(child: CircularProgressIndicator(),):
          Column(
          children: [
            Container(
              margin: EdgeInsets.fromLTRB(16, 16, 16, 16),
              height: 50,
              child: TextField(
                textAlign: TextAlign.right,
                controller: controller,
                onChanged: (search) => _searchChanged(search),
                decoration: InputDecoration(
                  suffixIcon: Icon(Icons.search),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                  )
                ),
              ),
            ),
            Expanded(
              child: ListView.builder(
                shrinkWrap: true,
                physics: const BouncingScrollPhysics(),
                itemCount:searcheduser.length,
                itemBuilder: (context, index) => Card(
                      elevation: 3,
                      child: ListTile(
                        onTap: (){},
                        trailing:CircleAvatar(child: Image.asset("assets/family.jpg"),backgroundColor: Colors.white,) ,
                      title:  Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                           Text(searcheduser.elementAt(index).name,style: TextStyle(color: Colors.black87),),
                          ],
                        ),
                        leading:IconButton(onPressed:(){
                          searcheduser.elementAt(index).isfamily==1?
                          Navigator.push(context, MaterialPageRoute(builder: (BuildContext ctx)=>MemberList(nationalnumber: searcheduser.elementAt(index).nationalNumber,isAdmin: true,))):
                          Navigator.push(context, MaterialPageRoute(builder: (BuildContext ctx)=>UserProfile(nationalnumber: searcheduser.elementAt(index).nationalNumber)));
    }        ,            icon: Icon(Icons.arrow_forward,color: Color(0xFF31B89C))),

                      ),
                    ),

                ),
              ),
          ],
        )

    );
  }

  void _searchChanged(String searchText) {
    if (searchText != null && searchText.isNotEmpty) {

      setState(() {
        searcheduser =
            List.from(_user.where((name) => name.name.contains(searchText) || name.nationalNumber.toString().contains(searchText)));
      });
    }
    else {
      setState(() {
        searcheduser =
            List.from(_user);
      });
    }
  }


  void getUserDetails() async {
    var list = await db.GetAllUsers();


    setState(() {
      _user = list.map((model) => User.fromMap(model)).toList();
      searcheduser = list.map((model) => User.fromMap(model)).toList();

      // for(int i =0;i<_user.length;i++)
      //   {
      //     allNames.add(_user[i].name);
      //     searchedNames.add(_user[i].name);
      //     print(allNames[i]);
      //     print(searchedNames[i]);
      //   }
    });
  }


}

