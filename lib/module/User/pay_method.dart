import 'package:flutter/material.dart';
import 'package:mohaned_project/model/user.dart';
import 'package:mohaned_project/module/User/bank_details.dart';
import 'package:mohaned_project/widget/background_image.dart';
import 'package:mohaned_project/widget/defualt_btn.dart';

class PayMethods extends StatefulWidget {

  final User user;
  final bool isRegister,isfamily;
  const PayMethods({Key key, this.user, this.isRegister, this.isfamily}) : super(key: key);

  @override
  _PayMethodsState createState() => _PayMethodsState();
}

class _PayMethodsState extends State<PayMethods> {


  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(

      appBar: AppBar(
        title: Text(" بيانات الدفع",style: TextStyle(color: Colors.black54,fontWeight: FontWeight.bold),),
        centerTitle: true,
        elevation: 2,
        backgroundColor: Colors.white,
        leading:  Container(
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage(
                'assets/logo.jpeg',
              ),
              fit: BoxFit.cover,
            ),
          ),

        ),
      ),

     body:    SingleChildScrollView(
       child: Column(
         mainAxisAlignment: MainAxisAlignment.start,
         children: [
           Image.asset(
             'assets/logo.jpeg',
             height: size.height*.25,
             width: size.width,
           ),
           Container(
             padding: EdgeInsets.only(
                 top: size.height * .15, right: size.width * .1),
             child: Row(
               mainAxisAlignment: MainAxisAlignment.end,
               children: [
                 Text(
                   "اختر طريقة الدفع",
                   style: TextStyle(
                       fontSize: 18, fontWeight: FontWeight.bold),
                 )
               ],
             ),
           ),

           Container(
               padding: EdgeInsets.only(top: 20,),
               child: Column(
                 mainAxisAlignment: MainAxisAlignment.end,
                 children: [
                   Row(
                     mainAxisAlignment: MainAxisAlignment.end,
                     children: [
                       DefaultButton(
                         onPress: ()
                         {
                           Navigator.push(context, MaterialPageRoute(builder: (BuildContext ctx)=>PayMentDetails(
                             isRegister: true,
                             user: widget.user,
                             paymethod: "bank",
                             isfamily: widget.isfamily,
                           )));
                         },
                         text: "البنك",
                         textColor:Color(0xFF31B89C) ,
                         width: 1,)
                     ],
                   ),
                   SizedBox(height: 5,),
                 ],
               )) ,
           Container(
               padding: EdgeInsets.only(top: 20,),
               child: Column(
                 mainAxisAlignment: MainAxisAlignment.end,
                 children: [
                   Row(
                     mainAxisAlignment: MainAxisAlignment.end,
                     children: [
                       DefaultButton(
                         onPress: ()
                         {
                           Navigator.push(context, MaterialPageRoute(builder: (BuildContext ctx)=>PayMentDetails(
                             isRegister: true,
                             user: widget.user,
                             paymethod: "bankcard",
                             isfamily: widget.isfamily,
                           )));
                         },
                         text: "بطاقة الصراف",
                         textColor:Color(0xFF31B89C) ,
                         width: 1,)
                     ],
                   ),
                   SizedBox(height: 5,),
                 ]
               )) ,
           Container(
               padding: EdgeInsets.only(top: 20,),
               child: Column(
                 mainAxisAlignment: MainAxisAlignment.end,
                 children: [
                   Row(
                     mainAxisAlignment: MainAxisAlignment.end,
                     children: [
                       DefaultButton(
                         onPress: ()
                         {
                           Navigator.push(context, MaterialPageRoute(builder: (BuildContext ctx)=>PayMentDetails(
                             isRegister: true,
                             user: widget.user,
                             paymethod: "simcard",
                             isfamily: widget.isfamily,
                           )));
                         },
                         text: "تحويل الرصيد",
                         textColor:Color(0xFF31B89C) ,
                         width: 1,)
                     ],
                   ),
                   SizedBox(height: 5,),

                 ],
               )) ,

         ],
       ),
     )
    );
  }


}
