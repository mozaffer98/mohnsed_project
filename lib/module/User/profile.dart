import 'package:flutter/material.dart';
import 'package:mohaned_project/model/user.dart';

import '../../local_db/local_db.dart';

class UserProfile extends StatefulWidget {
  final nationalnumber;
  const UserProfile({Key key, this.nationalnumber}) : super(key: key);

  @override
  _UserProfileState createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile> {


  var db=new DatabaseHelper();
  User _user;
@override
  void initState() {
    // TODO: implement initState
    super.initState();

    GetUser();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("تفصيلي",style: TextStyle(color: Colors.black54,fontWeight: FontWeight.bold),),
        centerTitle: true,
        elevation: 2,
        backgroundColor: Colors.white,
        leading:  Container(
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage(
                'assets/logo.jpeg',
              ),
              fit: BoxFit.cover,
            ),
          ),

        ),
      ),
      body:_user==null?Center(child: CircularProgressIndicator.adaptive()): SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Card(
              elevation: 5,
              margin: EdgeInsets.only(top: 50,right: 20,left: 20),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(child: Text("${_user.nationalNumber}")),

                        Container(child: Text("الرقم الوطني")),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(child: Text("${_user.name}")),

                        Container(child: Text("الاسم")),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(child: Text("${_user.phoneNumber}")),

                        Container(child: Text("رقم الهاتف")),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(child: Text("${_user.gender}")),

                        Container(child: Text("الجنس")),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(child: Text("${_user.familyMember}")),

                        Container(child: Text("عدد افراد الاسرة")),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(child: Text("${_user.address}")),

                        Container(child: Text("العنوان")),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(child: Text(_user.paymethod=="simcard"?"رصيد في الهاتف":_user.paymethod=="bank"?"الحساب البنكي":"بطاقة الصراف")),
                        Container(child: Text("طريقة الدفع")),
                      ],
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  void GetUser()async
  {
    User user=await db.GetUser(widget.nationalnumber);
    _user=user;
    setState(() {});
  }
}
