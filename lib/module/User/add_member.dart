import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:mohaned_project/local_db/local_db.dart';
import 'package:mohaned_project/local_db/member_local_db.dart';
import 'package:mohaned_project/model/family_member.dart';
import 'package:mohaned_project/model/user.dart';
import 'package:mohaned_project/module/User/member_list.dart';
import 'package:mohaned_project/module/User/pay_method.dart';
import 'package:mohaned_project/widget/background_image.dart';
import 'package:mohaned_project/widget/defualt_btn.dart';
import 'package:mohaned_project/widget/gradual.dart';
import 'package:mohaned_project/widget/text_field.dart';

class AddMember extends StatefulWidget {
  final bool isAdd,isRegister;
  final nationalnumber;
  final FamilyMember familyMember;
  final User user;
  const AddMember({Key key, this.isAdd, this.familyMember, this.nationalnumber, this.user, this.isRegister}) : super(key: key);

  @override
  _AddMemberState createState() => _AddMemberState();
}

class _AddMemberState extends State<AddMember> {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  TextEditingController _name_ctrl=TextEditingController();
  TextEditingController _national_number_ctrl=TextEditingController();
  int radioGroup = 2;
  List<String> kinships = ["ابن", "ابنة","ام","اب","اخ","اخت"];
  var _kinship,gender;


  var db=new DatabaseHelperMember();


  void radioEventHandler(int value){
    setState(() {
      radioGroup = value;
      print(radioGroup);
    });
  }
  Widget _myRadioButton({String title, int value,bool sel}) {
    return RadioListTile<int>(
      value:value ,
      groupValue: radioGroup,
      onChanged: radioEventHandler ,
      title: new Text(title),
      activeColor: Colors.blue,
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if(widget.isAdd==false)
      {
        _name_ctrl.text=widget.familyMember.name;
        _national_number_ctrl.text=widget.familyMember.nationalNumber.toString();
         radioGroup=widget.familyMember.gender=="ذكر"?2:1;
        _kinship=widget.familyMember.kinship;
      }
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return  Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        title: Text("إضافة فرد",style: TextStyle(color: Colors.black54,fontWeight: FontWeight.bold),),
        centerTitle: true,
        elevation: 2,
        backgroundColor: Colors.white,
        leading:  Container(
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage(
                'assets/logo.jpeg',
              ),
              fit: BoxFit.cover,
            ),
          ),

        ),
      ),
      body: Stack(
        alignment: AlignmentDirectional.centerStart,
        children: [
          const BackgroundImage1(),
          SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  padding: EdgeInsets.only(top: size.height*.028,right:size.width*.1 ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text("الاسم",style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold),)
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: size.height*.007,right:size.width*.1,left: size.width*.1 ),
                  child: DefaultTextFormField(
                    controller: _name_ctrl,
                    type: TextInputType.text,
                    hint: "",
                    validate: (value) {
                      if (value == null) {
                        return 'ادخل الاسم';
                      }
                      return null;

                    },
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: size.height*.028,right:size.width*.1 ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text("الرقم الوطني",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: size.height*.007,right:size.width*.1,left: size.width*.1 ),
                  child: DefaultTextFormField(
                    controller: _national_number_ctrl,
                    hint: "",
                    validate: (value) {
                      if (value == null) {
                        return 'ادخل الرقم الوطني';
                      }
                      return null;

                    },
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: size.height*.028,right:size.width*.1 ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text("النوع",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(left: size.height*.068 ),
                  child:Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    textDirection: TextDirection.ltr,
                    children: [
                      Expanded(
                        child: _myRadioButton(
                            title: "انثى",
                            value: 1,
                            sel: false
                        ),
                      ),
                      Expanded(
                        child: _myRadioButton(
                            title: "ذكر",
                            value: 2,
                            sel: true
                        ),
                      ),

                    ],),
                ),
                Container(
                  padding: EdgeInsets.only(top: size.height*.018,right:size.width*.1 ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text("صلة القرابة",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: size.height*.012,right:size.width*.1 ,left:size.width*.1 ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Expanded(
                        child: Container(
                          height: size.height *.095,
                          child: InputDecorator(
                            decoration: const InputDecoration(
                                border: OutlineInputBorder()),
                            child: DropdownButtonHideUnderline(
                              child: ButtonTheme(
                                  alignedDropdown: true,
                                  child: DropdownButton(
                                    hint:  Text(
                                      'اختر صلة القرابة',style: TextStyle(fontSize: 11,fontWeight: FontWeight.bold),), // Not necessary for Option 1
                                    value: _kinship,
                                    onChanged: (newValue) {
                                      setState(() {
                                        _kinship = newValue;
                                        print(_kinship);
                                      });
                                    },
                                    items: kinships.map((location) {
                                      return DropdownMenuItem(
                                        child: new Text(location),
                                        value: location,
                                      );
                                    }).toList(),
                                  )),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
            Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      widget.isRegister? Container(
                        width: size.width *.40,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color:Color(0xFF31B89C) ,
                        ),
                        child: MaterialButton(
                          onPressed: (){
                            Navigator.push(context, MaterialPageRoute(builder: (BuildContext ctx)=>PayMethods(isRegister: true,user: widget.user,)));
                          },
                          child: Text(
                            " التالي",
                            style: const TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                                fontFamily: 'Tajawal',
                                fontWeight: FontWeight.bold
                            ),
                          ),
                        ),
                      ):Container(),
                      Container(
                        width: size.width *.40,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color:Color(0xFF31B89C) ,
                        ),
                        child: MaterialButton(
                          onPressed: (){
                            validate();
                          },
                          child: Text(
                            widget.isAdd==true ? "إضافة":"تعديل",
                            style: const TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                                fontFamily: 'Tajawal',
                                fontWeight: FontWeight.bold
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                )


              ],
            ),
          ),
        ],
      ),
    );


  }

  void validate()
  {
    if(_name_ctrl.text.isEmpty){
        scaffoldKey.currentState.showSnackBar(
          SnackBar(
            content: Text("ادخل الاسم "),
          ),
        );

    }
    else
    if(_national_number_ctrl.text.isEmpty){
      scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text("ادخل الرقم الوطني "),
        ),
      );

    }
    else
    if(_kinship==null){
      scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text("اختر صلة القرابة"),
        ),
      );

    }
    else AddMember();
  }

  void AddMember()async
  {

    if(widget.isAdd ==true)
      {
        int res =await db.addMember(
            FamilyMember(
            name:_name_ctrl.text ,
            nationalNumber:int.parse( _national_number_ctrl.text) ,
            gender:radioGroup==2?"ذكر":"انثى" ,
            kinship:_kinship ,
            familynationalnumber: widget.nationalnumber
            ));
        if(res ==0)
        {

          EasyLoading.showError("لم تتم الاضافة",dismissOnTap: true,duration: Duration(minutes: 1));

        }
        else
        {
          EasyLoading.showSuccess(" تمت الاضافة",dismissOnTap: true,duration: Duration(seconds: 5));
          _name_ctrl.clear();
          _national_number_ctrl.clear();
          radioGroup=2;
          _kinship=null;
         // Navigator.pop(context);

        }
      }
    else
      {
        int res =await db.updateMember(FamilyMember(
           id: widget.familyMember.id,
            name:_name_ctrl.text ,
            nationalNumber:int.parse( _national_number_ctrl.text) ,
            gender:radioGroup==2?"ذكر":"انثى" ,
            kinship:_kinship ,
           familynationalnumber: widget.nationalnumber
        ));
        if(res ==0)
        {

          EasyLoading.showError("لم يتم التعديل",dismissOnTap: true,duration: Duration(minutes: 1));

        }
        else
        {
          EasyLoading.showSuccess(" تم التعديل",dismissOnTap: true,duration: Duration(seconds: 5));
          _name_ctrl.clear();
          _national_number_ctrl.clear();
          radioGroup=2;
          _kinship=null;
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (context) => MemberList(isAdmin: true,nationalnumber: widget.nationalnumber),
            ),
          );

        }
      }

      }


}
