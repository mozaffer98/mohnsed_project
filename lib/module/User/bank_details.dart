import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:mohaned_project/local_db/local_db.dart';
import 'package:mohaned_project/main.dart';
import 'package:mohaned_project/model/user.dart';
import 'package:mohaned_project/module/Setting.dart';
import 'package:mohaned_project/widget/background_image.dart';
import 'package:mohaned_project/widget/defualt_btn.dart';
import 'package:mohaned_project/widget/gradual.dart';
import 'package:mohaned_project/widget/text_field.dart';
import 'package:permission_handler/permission_handler.dart';

class PayMentDetails extends StatefulWidget {
  final bool isRegister,isfamily;
  final User user;
  final paymethod;
  const PayMentDetails({Key key, this.isRegister, this.user, this.paymethod, this.isfamily}) : super(key: key);

  @override
  _PayMentDetailsState createState() => _PayMentDetailsState();
}

class _PayMentDetailsState extends State<PayMentDetails> {
  final formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  TextEditingController _bankname_ctrl = TextEditingController();
  TextEditingController _accountNumber_ctrl = TextEditingController();
  TextEditingController _branchName_ctrl = TextEditingController();
  TextEditingController _bankcardnumber_ctrl = TextEditingController();
  TextEditingController _CompanyName_ctrl = TextEditingController();
  TextEditingController _phonenumber_ctrl = TextEditingController();
  var db = new DatabaseHelper(),_bank;
  List<User> _user;

  getPermission() async {
    if (await Permission.storage.request().isGranted) {}
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
   // getUserDetails();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      key: scaffoldKey,
      appBar: AppBar(
        title: Text("بيانات طريقة الدفع",style: TextStyle(color: Colors.black54,fontWeight: FontWeight.bold),),
        centerTitle: true,
        elevation: 2,
        backgroundColor: Colors.white,
        leading:  Container(
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage(
                'assets/logo.jpeg',
              ),
              fit: BoxFit.cover,
            ),
          ),

        ),
      ),
      body: Stack(
        alignment: AlignmentDirectional.centerStart,
        children: [
          const BackgroundImage1(),
          SingleChildScrollView(
                  //دا اللى هيشغلى اللى  validator
                  child:widget.paymethod=="bank"? bankmethod(size):widget.paymethod=="bankcard"?bankcardmethod(size):cardmethod(size),
                ),
        ],
      ),
    );
  }

  void vaildation() async {
    if (_bank==null &&
        _accountNumber_ctrl.text.isEmpty &&
        _branchName_ctrl.text.isEmpty) {
      scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text("لا يمكن ترك حقل فارغ"),
        ),
      );
    } else if (_bank==null) {
      scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text("ادخل اسم البنك"),
        ),
      );
    } else if (_accountNumber_ctrl.text.isEmpty) {
      scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text("حقل رقم الحساب فارغ"),
        ),
      );
    } else if (_branchName_ctrl.text.isEmpty) {
      scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text("حقل اسم الفرع فارغ"),
        ),
      );
    } else {
      submit();
    }
  }

  void submit() async {
    if (widget.isRegister == true) {

      if(widget.paymethod=="bank"){
        widget.user.bank = _bank;
        widget.user.accountNumber = int.parse(_accountNumber_ctrl.text);
        widget.user.branchName = _branchName_ctrl.text;
        widget.user.paymethod = widget.paymethod;
        widget.user.bankcardnumber = "";
        widget.user.companyname = "";
        widget.user.simnumber = "";
      }


      else if(widget.paymethod=="bankcard"){
        widget.user.bank = _bankname_ctrl.text;
        widget.user.accountNumber = 0;
        widget.user.branchName = _branchName_ctrl.text;
        widget.user.paymethod = widget.paymethod;
        widget.user.bankcardnumber =_bankcardnumber_ctrl.text;
        widget.user.companyname = "";
        widget.user.simnumber = "";
      }
      else{
        widget.user.bank ="";
        widget.user.accountNumber = 0;
        widget.user.branchName = "";
        widget.user.paymethod = widget.paymethod;
        widget.user.bankcardnumber = "";
        widget.user.companyname = _CompanyName_ctrl.text;
        widget.user.simnumber = _phonenumber_ctrl.text;
      }



      int res = await db.addUser(widget.user);
      print(res);

      if (res == 0 || res == null) {
        EasyLoading.showError("تم التسجيل مسبقا",
            dismissOnTap: true, duration: Duration(seconds: 7));
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (BuildContext ctx) => HomePage()),
            (route) => false);
      } else {
        EasyLoading.showSuccess("تم التسجيل",
            dismissOnTap: true, duration: Duration(seconds: 7));

        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (BuildContext ctx) =>Setting(nationalnumber: widget.user.nationalNumber,isFamily: widget.user.isfamily==0?false:true,)),
            (route) => false);
      }




    } else {

      if(widget.user.paymethod=="bank"){
        widget.user.bank = _bank;
        widget.user.accountNumber = int.parse(_accountNumber_ctrl.text);
        widget.user.branchName = _branchName_ctrl.text;
        widget.user.paymethod = widget.paymethod;
        widget.user.bankcardnumber = "";
        widget.user.companyname = "";
        widget.user.simnumber = "";
      }


      else if(widget.user.paymethod=="bankcard"){
        widget.user.bank = _bankname_ctrl.text;
        widget.user.accountNumber = 0;
        widget.user.branchName = _branchName_ctrl.text;
        widget.user.paymethod = widget.paymethod;
        widget.user.bankcardnumber =_bankcardnumber_ctrl.text;
        widget.user.companyname = "";
        widget.user.simnumber = "";
      }
      else{
        widget.user.bank ="";
        widget.user.accountNumber = 0;
        widget.user.branchName = "";
        widget.user.paymethod = widget.paymethod;
        widget.user.bankcardnumber = "";
        widget.user.companyname = _CompanyName_ctrl.text;
        widget.user.simnumber = _phonenumber_ctrl.text;
      }



      int res = await db.updateUser(widget.user);
      print(widget.user.id);
      if (res == 0) {
        EasyLoading.showError("لم يتم التعديل");
      } else {
        EasyLoading.showSuccess("تم التعديل");
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (BuildContext ctx) => Setting(
              nationalnumber: widget.user.nationalNumber,
              isFamily: widget.user.isfamily==1?true:false,
            )),
            (route) => false);
      }
    }
  }

  void getUserDetails() async {
    setState(() {
      if (widget.isRegister == false) {
        if(widget.user.paymethod=="bank")
          {
            _bank =widget.user.bank;
            _accountNumber_ctrl.text =  widget.user.accountNumber.toString();
            _branchName_ctrl.text = widget.user.branchName;
          }
        else if(widget.user.paymethod=="bankcard")
          {
            _bank =widget.user.bank;
            _bankcardnumber_ctrl.text=widget.user.bankcardnumber;
            _branchName_ctrl.text=widget.user.branchName;

          }
        else{
          _CompanyName_ctrl.text=widget.user.companyname;
          _phonenumber_ctrl.text=widget.user.simnumber;
        }

      }
    });
  }

















  Widget bankmethod(final size)
  {
return Column(
  mainAxisAlignment: MainAxisAlignment.start,
  children: [

    Container(
      padding: EdgeInsets.only(
           right: size.width * .1),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Text(
            "اسم البنك",
            style: TextStyle(
                fontSize: 18, fontWeight: FontWeight.bold),
          )
        ],
      ),
    ),
    Container(
      padding: EdgeInsets.only(
          top: size.height * .012,
          right: size.width * .01,
          left: size.width * .01
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Expanded(
            child: Container(
              height: size.height * .095,
              margin: EdgeInsets.only(  right: size.width * .07),
              child: InputDecorator(
                decoration: const InputDecoration(
                    border: OutlineInputBorder()),
                child: DropdownButtonHideUnderline(
                  child: ButtonTheme(
                      alignedDropdown: true,
                      child: DropdownButton(
                        hint: Text(
                          "البنك",
                          style: TextStyle(
                              fontSize: 11,
                              fontWeight: FontWeight.bold),
                        ), // Not necessary for Option 1
                        value: _bank,
                        onChanged: (newValue) {
                          setState(() {
                            _bank = newValue;
                            print(_bank);
                          });
                        },
                        items:
                        banks.map((familynumber) {
                          return DropdownMenuItem(
                            child:
                            new Text("${familynumber}"),
                            value: familynumber,
                          );
                        }).toList(),
                      )),
                ),
              ),
            ),
          ),
        ],
      ),
    ),
    Container(
      padding: EdgeInsets.only(
          top: size.height * .012, right: size.width * .1),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Text(
            "رقم الحساب",
            style: TextStyle(
                fontSize: 18, fontWeight: FontWeight.bold),
          )
        ],
      ),
    ),
    Container(
      padding: EdgeInsets.only(
          top: size.height * .007,
          right: size.width * .07,
          left: size.width * .01),
      child: DefaultTextFormField(
        type: TextInputType.numberWithOptions(),
        controller: _accountNumber_ctrl,
        hint: "",
        validate: (value) {
          if (value == null) {
            return 'ادخل رقم الحساب';
          }
          return null;
        },
      ),
    ),
    Container(
      padding: EdgeInsets.only(
          top: size.height * .012, right: size.width * .1),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Text(
            "اسم الفرع",
            style: TextStyle(
                fontSize: 18, fontWeight: FontWeight.bold),
          )
        ],
      ),
    ),
    //
    Container(
      padding: EdgeInsets.only(
          top: size.height * .007,
          right: size.width * .07,
          left: size.width * .01),
      child: DefaultTextFormField(
        controller: _branchName_ctrl,
        type: TextInputType.text,
        hint: "",
        validate: (value) {
          if (value == null) {
            return 'ادخل اسم الفرع';
          }
          return null;
        },
      ),
    ),
    Container(
        padding: EdgeInsets.only(
          top: size.height * .099,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                DefaultButton(
                  onPress: vaildation,
                  textColor: Color(0xFF31B89C),
                  text: widget.isRegister == true
                      ? "تسجيل"
                      : "تعديل",
                  width: 1,
                )
              ],
            ),
            SizedBox(
              height: 5,
            ),
          ],
        ))
  ],
);
  }


  Widget  bankcardmethod(final size)
  {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.only(right: size.width * .1),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Text(
                "اسم البنك",
                style: TextStyle(
                    fontSize: 18, fontWeight: FontWeight.bold),
              )
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.only(
              top: size.height * .012,
              right: size.width * .01,
              left: size.width * .01
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Expanded(
                child: Container(
                  height: size.height * .095,
                  margin: EdgeInsets.only(  right: size.width * .07),
                  child: InputDecorator(
                    decoration: const InputDecoration(
                        border: OutlineInputBorder()),
                    child: DropdownButtonHideUnderline(
                      child: ButtonTheme(
                          alignedDropdown: true,
                          child: DropdownButton(
                            hint: Text(
                              "البنك",
                              style: TextStyle(
                                  fontSize: 11,
                                  fontWeight: FontWeight.bold),
                            ), // Not necessary for Option 1
                            value: _bank,
                            onChanged: (newValue) {
                              setState(() {
                                _bank = newValue;
                                print(_bank);
                              });
                            },
                            items:
                            banks.map((familynumber) {
                              return DropdownMenuItem(
                                child:
                                new Text("${familynumber}"),
                                value: familynumber,
                              );
                            }).toList(),
                          )),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.only(
              top: size.height * .012, right: size.width * .1),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Text(
                "اسم الفرع",
                style: TextStyle(
                    fontSize: 18, fontWeight: FontWeight.bold),
              )
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.only(
              top: size.height * .007,
              right: size.width * .07,
              left: size.width * .01),
          child: DefaultTextFormField(
            type: TextInputType.text,
            controller: _branchName_ctrl,
            hint: "",
            validate: (value) {
              if (value == null) {
                return '';
              }
              return null;
            },
          ),
        ),
        Container(
          padding: EdgeInsets.only(
              top: size.height * .012, right: size.width * .1),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Text(
                "رقم بطاقة الصراف",
                style: TextStyle(
                    fontSize: 18, fontWeight: FontWeight.bold),
              )
            ],
          ),
        ),
        //
        Container(
          padding: EdgeInsets.only(
              top: size.height * .007,
              right: size.width * .07,
              left: size.width * .01),
          child: DefaultTextFormField(
            controller: _bankcardnumber_ctrl,
            type: TextInputType.number,
            hint: "",
            validate: (value) {
              if (value == null) {
                return '';
              }
              return null;
            },
          ),
        ),
        Container(
            padding: EdgeInsets.only(
              top: size.height * .099,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    DefaultButton(
                      onPress: (){
                        if (_bank==null &&
                            _bankcardnumber_ctrl.text.isEmpty &&
                            _branchName_ctrl.text.isEmpty) {
                          scaffoldKey.currentState.showSnackBar(
                            SnackBar(
                              content: Text("لا يمكن ترك حقل فارغ"),
                            ),
                          );
                        }
                        else if (_bank==null) {
                          scaffoldKey.currentState.showSnackBar(
                            SnackBar(
                              content: Text("ادخل اسم البنك"),
                            ),
                          );
                        } else if (_bankcardnumber_ctrl.text.isEmpty) {
                          scaffoldKey.currentState.showSnackBar(
                            SnackBar(
                              content: Text("حقل رقم البطاقة فارغ"),
                            ),
                          );
                        } else if (_branchName_ctrl.text.isEmpty) {
                          scaffoldKey.currentState.showSnackBar(
                            SnackBar(
                              content: Text("حقل اسم الفرع فارغ"),
                            ),
                          );
                        }
                        else
                          {
                            submit();
                          }
                      },
                      textColor: Color(0xFF31B89C),
                      text: widget.isRegister == true
                          ? "تسجيل"
                          : "تعديل",
                      width: 1,
                    )
                  ],
                ),
                SizedBox(
                  height: 5,
                ),
              ],
            ))
      ],
    );
  }


  Widget cardmethod(final size)
  {
    return Column(
      children: [
        Container(
          padding: EdgeInsets.only(
              top: size.height * .012, right: size.width * .1),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Text(
                "شركة الاتصالات",
                style: TextStyle(
                    fontSize: 18, fontWeight: FontWeight.bold),
              )
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.only(
              top: size.height * .007,
              right: size.width * .07,
              left: size.width * .01),
          child: DefaultTextFormField(
            type: TextInputType.text,
            controller: _CompanyName_ctrl,
            hint: "",
            validate: (value) {
              if (value == null) {
                return '';
              }
              return null;
            },
          ),
        ),
        Container(
          padding: EdgeInsets.only(
              top: size.height * .012, right: size.width * .1),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Text(
                "رقم الهاتف",
                style: TextStyle(
                    fontSize: 18, fontWeight: FontWeight.bold),
              )
            ],
          ),
        ),
        //
        Container(
          padding: EdgeInsets.only(
              top: size.height * .007,
              right: size.width * .07,
              left: size.width * .01),
          child: DefaultTextFormField(
            controller: _phonenumber_ctrl,
            type: TextInputType.phone,
            hint: "",
            validate: (value) {
              if (value == null) {
                return '';
              }
              return null;
            },
          ),
        ),
        Container(
            padding: EdgeInsets.only(
              top: size.height * .099,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    DefaultButton(
                      onPress: (){
                        if (
                            _CompanyName_ctrl.text.isEmpty &&
                            _phonenumber_ctrl.text.isEmpty) {
                          scaffoldKey.currentState.showSnackBar(
                            SnackBar(
                              content: Text("لا يمكن ترك حقل فارغ"),
                            ),
                          );
                        }
                        else if (_CompanyName_ctrl.text.isEmpty) {
                          scaffoldKey.currentState.showSnackBar(
                            SnackBar(
                              content: Text("ادخل اسم الشركة"),
                            ),
                          );
                        } else if (_phonenumber_ctrl.text.isEmpty) {
                          scaffoldKey.currentState.showSnackBar(
                            SnackBar(
                              content: Text("حقل رقم الهاتف فارغ"),
                            ),
                          );
                        }
                        else
                          {
                            submit();
                          }

                      },
                      textColor: Color(0xFF31B89C),
                      text: widget.isRegister == true
                          ? "تسجيل"
                          : "تعديل",
                      width: 1,
                    )
                  ],
                ),
                SizedBox(
                  height: 5,
                ),
              ],
            ))
      ],
    );
  }


  List<String>banks=["بنك الثروة الحيوانية",
    " مصرف التنمية الصناعية",
    " بنك ام درمان الوطني",
    "بنك الاستثمار المالي",
    "بنك التضامن الاسلامي",
    "بنك بيبلوس افريقيا",
    "بنك العمال الوطني",
    "بنك فيصل الاسلامي السوداني",
    "بنك الزراعي السوداني",
    "مصرف الساحل والصحراء للاستثمار والتجارة",
    "البنك السوداني المصري",
    "مصرف البلد",
    "بنك آيفوري",
    "بنك النيل للتجارة والخدمات",
    "البنك العقاري التجاري",
    "بنك البركة السوداني",
    "البنك السوداني الفرنسي",
    "البنك السعودي السوداني",
    "بنك النيل الازرق المشرق",
    "بنك النيلين",
    "مصرف المزارع التجاري",
    "بنك الجزيرة السوداني الاردني",
    "بنك قطر الوطني",
    "بنك المال المتحد",
    "بنك تنمية الصادرات",
    "البنك الأهلي السوداني",
    "مصرف السلام",
    "بنك الاسرة",
    "مصرف الادخار والتنمية الاجتماعية",
    "بنك الخرطوم",
    "البنك الاسلامي السوداني",
    "بنك ابوظبي الوطني",
    "البنك العربي السوداني",
    "البنك الاهلي المصري (الخرطوم)",
    "مصرف قطر الآسلامي",
    "مصرف أبوظبي الإسلامي",
    "بنك الخليج",
    "بنك زراعات كاتيليم"
  ];
}
