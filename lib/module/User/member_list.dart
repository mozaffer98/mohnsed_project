import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:mohaned_project/local_db/member_local_db.dart';
import 'package:mohaned_project/model/family_member.dart';
import 'package:mohaned_project/module/User/add_member.dart';
import 'package:mohaned_project/widget/member.dart';

class MemberList extends StatefulWidget {
  final nationalnumber;
  final bool isAdmin;

  const MemberList({Key key, this.nationalnumber, this.isAdmin}) : super(key: key);

  @override
  _MemberListState createState() => _MemberListState();
}

class _MemberListState extends State<MemberList> {

 List<FamilyMember>  _familyMember;
 var db=DatabaseHelperMember();

 @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getallmember();
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("اعضاء الاسرة",style: TextStyle(color: Colors.black54,fontWeight: FontWeight.bold),),
        centerTitle: true,
        elevation: 2,
        backgroundColor: Colors.white,
        leading:  Container(
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage(
                'assets/logo.jpeg',
              ),
              fit: BoxFit.cover,
            ),
          ),

        ),
      ),
        body:_familyMember == null? Center(child: CircularProgressIndicator(),): Column(
          children: [
            Container(
              margin: EdgeInsets.only(right:20,top: 30 ),
                child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Text("${widget.nationalnumber}",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),
                SizedBox(width: 5,),
                Text(": الرقم الوطني",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),
              ],
            )),
            Expanded(
              child: ListView.builder(
                shrinkWrap: true,
                physics: const BouncingScrollPhysics(),
                itemCount: _familyMember.length,
                itemBuilder: (context, index) =>  MemberItem(
                  name: _familyMember.elementAt(index).name,
                  gender:_familyMember.elementAt(index).gender ,
                  kinship: _familyMember.elementAt(index).kinship,
                  nationalNumber: _familyMember.elementAt(index).nationalNumber,
                  onPressEdit:()=> Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) =>AddMember(isAdd: false,familyMember:_familyMember.elementAt(index) ,nationalnumber: widget.nationalnumber,isRegister: false),),),
                  onPressRemove: ()=> showAlert(context, 'هل تريد حذف العضو؟',_familyMember.elementAt(index).id),
                  isAdmin: widget.isAdmin,
                ),
              ),
            ),
          ],
        )

    );
  }
  void getallmember() async
  {
    var list = await db.GetAllMember(widget.nationalnumber);
    _familyMember = list.map((model) => FamilyMember.fromMap(model)).toList();
    setState(() {});
  }

 void showAlert(BuildContext context,String massage,int id) {
   showDialog(
       context: context,
       builder: (context) => AlertDialog(
         content:Text(massage,textDirection: TextDirection.rtl,),
         actions: [
           const SizedBox(
             width: 10,
           ),
           Center(
             child: Container(
               alignment: Alignment.center,
               width: MediaQuery.of(context).size.width * .60,
               decoration: const BoxDecoration(
                 borderRadius: BorderRadius.all(Radius.circular(20)),
               ),
               child: Row(
                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                 children: [
                   TextButton(
                       onPressed: (){
                         Navigator.pop(context);
                       },
                       child: Text('لا',style: TextStyle(color: Color(0xffEE4B5B)),)),
                   SizedBox(width: 2,),

                   TextButton(
                       onPressed: ()async{
                         int res=await db.deleteMember(id);
                         if(res ==0 ||res ==null)
                           {
                             EasyLoading.showError("لم يتم الحذف",);
                           }
                         else
                           {
                             getallmember();
                             EasyLoading.showSuccess(" تم الحذف",dismissOnTap: true,duration: Duration(seconds: 5));


                           }
                         Navigator.pop(context);
                       },
                       child: Text('نعم',style: TextStyle( color:Color(0xFF31B89C),
                       ),)),
                 ],
               ),
             ),
           ),
         ],
         // backgroundColor: AppBrand.mainColor,
       ));
 }
}
