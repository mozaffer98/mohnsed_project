import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:mohaned_project/local_db/local_db.dart';
import 'package:mohaned_project/main.dart';
import 'package:mohaned_project/model/user.dart';
import 'package:mohaned_project/module/User/add_member.dart';
import 'package:mohaned_project/module/User/bank_details.dart';
import 'package:mohaned_project/module/User/pay_method.dart';
import 'package:mohaned_project/widget/background_image.dart';
import 'package:mohaned_project/widget/defualt_btn.dart';
import 'package:mohaned_project/widget/gradual.dart';
import 'package:mohaned_project/widget/text_field.dart';

class UserDetails extends StatefulWidget {
  final nationalnumber,reg_type;
  final bool isRegister;

  UserDetails({this.nationalnumber, this.isRegister, this.reg_type});

  @override
  _UserDetailsState createState() => _UserDetailsState();
}

class _UserDetailsState extends State<UserDetails> {
  TextEditingController _name_ctrl = TextEditingController();
  TextEditingController _phone_name_ctrl = TextEditingController();
  TextEditingController _address_ctrl = TextEditingController();
  TextEditingController _family_member_ctrl = TextEditingController();
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  final formKey = GlobalKey<FormState>();
  List<String> gender = ["ذكر", "انثى"];

  User _user;
  var db = new DatabaseHelper();

  var _gender;
  List<int> familynumber = [2, 3, 4, 5, 6, 7, 8, 9, 10];
  int _familynumber;
  int national_number;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    //print(widget.nationalnumber);
    if (widget.isRegister == true)
      national_number = widget.nationalnumber;

    if(widget.isRegister==false)
      {
        getUserDetails();
      }
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Colors.white,
      key: scaffoldKey,
      appBar: AppBar(
        title: Text("بيانات المستخدم",style: TextStyle(color: Colors.black54,fontWeight: FontWeight.bold),),
        centerTitle: true,
        elevation: 2,
        backgroundColor: Colors.white,
        leading:  Container(
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage(
                'assets/logo.jpeg',
              ),
              fit: BoxFit.cover,
            ),
          ),

        ),
      ),
      body: Stack(
        alignment: AlignmentDirectional.centerStart,
        children: [
          const BackgroundImage1(),
          _user == null && widget.isRegister == false ? Center(child: CircularProgressIndicator(),)
              : SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        padding: EdgeInsets.only(
                            top: size.height * .03, right: size.width * .1),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(
                              "${national_number}",
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.bold),
                            ),
                            Text(
                              ":الرقم الوطني",
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(
                            top: size.height * .028, right: size.width * .1),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(
                              "الاسم",
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(
                            top: size.height * .007,
                            right: size.width * .1,
                            left: size.width * .1),
                        child: DefaultTextFormField(
                          controller: _name_ctrl,
                          type: TextInputType.name,
                          hint: "",
                          validate: (value) {
                            if (value == null) {
                              return 'ادخل الاسم';
                            }
                            return null;
                          },
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(
                            top: size.height * .012, right: size.width * .1),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(
                              "رقم الهاتف",
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(
                            top: size.height * .007,
                            right: size.width * .1,
                            left: size.width * .1),
                        child: DefaultTextFormField(
                          controller: _phone_name_ctrl,
                          hint: "",
                          validate: (value) {
                            if (value == null) {
                              return 'ادخل رقم الهاتف';
                            }
                            return null;
                          },
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(
                            top: size.height * .012, right: size.width * .1),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(
                              "النوع",
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(
                            top: size.height * .012,
                            right: size.width * .1,
                            left: size.width * .50),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Expanded(
                              child: Container(
                                height: size.height * .095,
                                child: InputDecorator(
                                  decoration: const InputDecoration(
                                      border: OutlineInputBorder()),
                                  child: DropdownButtonHideUnderline(
                                    child: ButtonTheme(
                                        alignedDropdown: true,
                                        child: DropdownButton(
                                          hint: Text(
                                            'اختر النوع',
                                            style: TextStyle(
                                                fontSize: 11,
                                                fontWeight: FontWeight.bold),
                                          ), // Not necessary for Option 1
                                          value: _gender,
                                          onChanged: (newValue) {
                                            setState(() {
                                              _gender = newValue;
                                              print(_gender);
                                            });
                                          },
                                          items: gender.map((location) {
                                            return DropdownMenuItem(
                                              child: new Text(location),
                                              value: location,
                                            );
                                          }).toList(),
                                        )),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(
                            top: size.height * .012, right: size.width * .1),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(
                              "العنوان",
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(
                            top: size.height * .007,
                            right: size.width * .1,
                            left: size.width * .1),
                        child: DefaultTextFormField(
                          type: TextInputType.text,
                          controller: _address_ctrl,
                          hint: "",
                          validate: (value) {
                            if (value == null) {
                              return 'ادخل العنوان';
                            }
                            return null;
                          },
                        ),
                      ),
                 if(widget.reg_type=="family")
                   Container(
                        padding: EdgeInsets.only(
                            top: size.height * .012, right: size.width * .1),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(
                              "عدد افراد الاسرة",
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.bold),
                            )
                          ],
                        ),
                      ),
                      if(widget.reg_type=="family")
                        Container(
                        padding: EdgeInsets.only(
                            top: size.height * .007,
                            right: size.width * .1,
                            left: size.width * .1),
                        child: DefaultTextFormField(
                          type: TextInputType.number,
                          controller: _family_member_ctrl,
                          hint: "",

                          validate: (value) {
                            if (value == null) {
                              return '';
                            }
                            return null;
                          },
                        ),
                      ),
                      Container(
                          padding: EdgeInsets.only(
                            top: size.height * .02,
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  DefaultButton(
                                    onPress: vaildation,
                                    textColor: Color(0xFF31B89C),
                                    text: widget.isRegister == true
                                        ? "التالي"
                                        : "تعديل",
                                    width: 1,

                                  )
                                ],
                              ),
                              SizedBox(
                                height: 5,
                              ),
                            ],
                          ))
                    ],
                  ),
                ),
        ],
      ),
      // floatingActionButton:    Container(
      //     padding: EdgeInsets.only(top: size.height*.02),
      //     //    width:size.width*.75 ,
      //     child: Row(
      //       mainAxisAlignment: MainAxisAlignment.start,
      //       children: [
      //         Container(
      //           width:size.width*.40 ,
      //           color: Color(0xFF31B89C),
      //           child: TextButton(
      //             child: Text(
      //                 "اضافة فرد".toUpperCase(),
      //                 style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold)
      //             ),
      //             style: ButtonStyle(
      //                 padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.all(15)),
      //                 foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
      //                 shape: MaterialStateProperty.all<RoundedRectangleBorder>(
      //                     RoundedRectangleBorder(
      //                         borderRadius: BorderRadius.only(bottomRight: Radius.circular(8),topRight: Radius.circular(8)),
      //                         side: BorderSide(color: Color(0xFF31B89C))
      //                     )
      //                 )
      //             ),
      //             onPressed: () =>  Navigator.push(context, MaterialPageRoute(builder: (BuildContext ctx)=>AddMember(isAdd: true,))),
      //           ),
      //         ),
      //       ],
      //     )
      // )
    );
  }

  void vaildation() async {
    if (_name_ctrl.text.isEmpty &&
        _phone_name_ctrl.text.isEmpty &&
        _address_ctrl.text.isEmpty) {
      scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text("لا يمكن ترك حقل فارغ"),
        ),
      );
    } else if (_name_ctrl.text.isEmpty) {
      scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text("ادخل اسمك "),
        ),
      );
    } else if (_phone_name_ctrl.text.isEmpty) {
      scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text("حقل رقم الهاتف"),
        ),
      );
    } else if (_gender == null) {
      scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text("اختر النوع"),
        ),
      );
    } else if (_address_ctrl.text.isEmpty) {
      scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text("ادخل العنوان"),
        ),
      );
    } else if (_family_member_ctrl.text.isEmpty  &&widget.reg_type=="family") {
      scaffoldKey.currentState.showSnackBar(
        SnackBar(
          content: Text("ادخل عدد افراد الاسرة"),
        ),
      );
    } else {
      submit();
    }
  }

  void submit() async {
    if (widget.isRegister == true)
    {
      User user = User(
        nationalNumber: widget.nationalnumber,
        name: _name_ctrl.text,
        phoneNumber: int.parse(_phone_name_ctrl.text),
        gender: _gender,
        isfamily: widget.reg_type=="single"? 0:1,
        address: _address_ctrl.text,
        familyMember:widget.reg_type=="single"? 0: int.parse(_family_member_ctrl.text),
      );


    if(widget.reg_type=="single")
      {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (BuildContext ctx) => PayMethods(
                  isRegister : true,
                  user: user,
                  isfamily: false,

                )));
      }
      else if((widget.reg_type=="family")){
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (BuildContext ctx) => AddMember(
                isAdd : true,
                user: user,
                isRegister: true,
                nationalnumber: widget.nationalnumber,
              )));
    }


    }

    else {
      _user.nationalNumber = national_number;
      _user.name = _name_ctrl.text;
      _user.phoneNumber = int.parse(_phone_name_ctrl.text);
      _user.address = _address_ctrl.text;
      _user.gender = _gender;
      _user.familyMember =_user.isfamily==1? int.parse(_family_member_ctrl.text):0;
      _user.bank = _user.bank;
      _user.accountNumber = _user.accountNumber;
      _user.branchName = _user.branchName;

      Navigator.push(context, MaterialPageRoute(builder: (BuildContext ctx)=>PayMentDetails(
        isRegister: false,
        user: _user,
        paymethod: _user.paymethod,
        isfamily: _user.isfamily==1?true:false,
      )));

      // int res = await db.updateUser(_user);
      // print(_user.id);
      // if (res == 0) {
      //   EasyLoading.showError("لم يتم التعديل");
      // } else {
      //   EasyLoading.showSuccess("تم التعديل");
      //   Navigator.pushAndRemoveUntil(
      //       context,
      //       MaterialPageRoute(builder: (BuildContext ctx) => HomePage()),
      //       (route) => false);
      // }
    }
  }

  void getUserDetails() async {
    var user = await db.GetUser(widget.nationalnumber);
    _user =  user;
    setState(() {
      if (widget.isRegister == false) {
        national_number = _user.nationalNumber;
        _name_ctrl.text = _user.name;
        _phone_name_ctrl.text = _user.phoneNumber.toString();
        _address_ctrl.text = _user.address;
        _gender = _user.gender;
        _family_member_ctrl.text = _user.familyMember.toString();
        print(widget.reg_type);

      }
    });
  }
}
