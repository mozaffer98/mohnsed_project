import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:mohaned_project/local_db/member_local_db.dart';
import 'package:mohaned_project/model/suggestion.dart';
import 'package:mohaned_project/widget/background_image.dart';
import 'package:mohaned_project/widget/defualt_btn.dart';
import 'package:mohaned_project/widget/gradual.dart';
import 'package:mohaned_project/widget/text_field.dart';


class Suggestions extends StatefulWidget {

  final nationalnumber;

  const Suggestions({Key key, this.nationalnumber}) : super(key: key);
  @override
  _SuggestionsState createState() => _SuggestionsState();
}

class _SuggestionsState extends State<Suggestions> {
  TextEditingController _suggest_ctrl=TextEditingController();
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  var db=new DatabaseHelperMember();

  bool isloading=false;
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        title: Text("ارسال شكوى",style: TextStyle(color: Colors.black54,fontWeight: FontWeight.bold),),
        centerTitle: true,
        elevation: 2,
        backgroundColor: Colors.white,
        leading:  Container(
          decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage(
                'assets/logo.jpeg',
              ),
              fit: BoxFit.cover,
            ),
          ),

        ),
      ),
      body: Stack(
        alignment: AlignmentDirectional.centerStart,
        children: [
          const BackgroundImage1(),
           SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                    height: size.height*.30,
                    margin:EdgeInsets.only(top: size.height*.24) ,
                    child: DefaultTextFormField1(hint: "الشكوى", controller: _suggest_ctrl,type:TextInputType.text ,)),
                Container(
                  child:  isloading==false?DefaultButton(
                    text: 'ارسال',
                    textColor:  Color(0xFF31B89C),
                    onPress:  (){
                      if (_suggest_ctrl.text.isEmpty) {
                        scaffoldKey.currentState.showSnackBar(
                          SnackBar(
                            content: Text("ادخل الشكوى "),
                          ),
                        );
                      }
                      else
                      {
                        addSuggestion();

                      }

                    },
                  ):Center(child: CircularProgressIndicator()),)
              ],
            ),
          ),
        ],
      ),


    );
  }

  void addSuggestion()async
  {
    int res =await db.addSuggestion(
        Suggestion(nationalnumber: widget.nationalnumber,suggestion:_suggest_ctrl.text )
       );
    if(res ==0)
    {

      EasyLoading.showError("لم تتم الاضافة",dismissOnTap: true,duration: Duration(minutes: 1));

    }
    else
    {

      _suggest_ctrl.clear();
      EasyLoading.showSuccess("تم إرسال الشكوى");
      Navigator.pop(context);

    }
  }
}
