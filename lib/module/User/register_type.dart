import 'package:flutter/material.dart';
import 'package:mohaned_project/module/User/user_details.dart';
import 'package:mohaned_project/widget/defualt_btn.dart';

class RegisterType extends StatefulWidget {
  final national_number;
  const RegisterType({Key key, this.national_number}) : super(key: key);

  @override
  _RegisterTypeState createState() => _RegisterTypeState();
}

class _RegisterTypeState extends State<RegisterType> {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
        appBar: AppBar(
          title: Text("طريقة التسجيل",style: TextStyle(color: Colors.black54,fontWeight: FontWeight.bold),),
          centerTitle: true,
          elevation: 2,
          backgroundColor: Colors.white,
          leading:  Container(
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage(
                  'assets/logo.jpeg',
                ),
                fit: BoxFit.cover,
              ),
            ),

          ),
        ),
        body:    SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Image.asset(
                'assets/logo.jpeg',
                height: size.height*.25,
                width: size.width,
              ),
              Container(
                padding: EdgeInsets.only(
                    top: size.height * .15, right: size.width * .1),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text(
                      "اختر طريقة التسجيل",
                      style: TextStyle(
                          fontSize: 18, fontWeight: FontWeight.bold),
                    )
                  ],
                ),
              ),

              Container(
                  padding: EdgeInsets.only(top: 20,),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          DefaultButton(
                            onPress: ()
                            {
                              Navigator.push(context, MaterialPageRoute(builder: (BuildContext ctx)=>UserDetails(
                                nationalnumber: widget.national_number,
                                isRegister: true,
                                reg_type: "single",

                              )));
                            },
                            text: "تسجيل كفرد ",
                            textColor:Color(0xFF31B89C) ,
                            width: 1,)
                        ],
                      ),
                      SizedBox(height: 5,),
                    ],
                  )) ,
              Container(
                  padding: EdgeInsets.only(top: 20,),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            DefaultButton(
                      onPress: ()
                  {
                  Navigator.push(context, MaterialPageRoute(builder: (BuildContext ctx)=>UserDetails(
                  nationalnumber:widget.national_number ,
                  isRegister:true ,
                    reg_type: "family",
                  )));
                  },
                              text: "عائلة",
                              textColor:Color(0xFF31B89C) ,
                              width: 1,)
                          ],
                        ),
                        SizedBox(height: 5,),
                      ]
                  )) ,


            ],
          ),
        )
    );
  }
}
